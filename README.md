# Streetmap of Düsseldorf, Germany
This is a small project creating a nice-looking streetmap of the city of 
Düsseldorf, Germany. Just run the files in the `R` directory in numerical order
to reproduce the plot.

It is based on a tutorial from ggplot2tutor:
  https://ggplot2tutor.com/streetmaps/streetmaps/

Adaptations made are outlined on my website there (as of 2019-12-21): 
  https://hohenfeld.is/posts/streetmaps-in-r-with-ggplot2/
  
Code is licensed under CC BY-NC-SA 4.0 see file LICENCE.
